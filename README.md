# dudelbox

![](dudelbox-usb.jpg)

## Hardware
  - USB Netzteil mit USB Typ A Buchse (>= 2A) [🛒 8€](https://amzn.to/34cdAqN)
  - Pi Zero W (per Micro-USB Kabel am Netzteil) 🛒 12€
  - SanDisk Ultra 32GB microSDHC UHS-I Class 10 [🛒 8€](https://amzn.to/3dUmoG9)
  - Passiver USB Hub (per USB OTG Adapter am Pi) [🛒 6€](https://amzn.to/2V5lbDu) [🛒 2€](https://amzn.to/2RdkR4m) oder Expansion Board 4 Port Hub [🛒 6€](https://amzn.to/2TRpyW5)
  - USB Speaker mit eingebauter Soundkarte am USB Hub [🛒 8€](https://amzn.to/2yzAqNf)
  - Card Reader 125 kHz (arbeitet quasi als Tastatur) am USB Hub [🛒 12€](https://amzn.to/3dYNbkT) Amazon DE / 5€ bei eBay aus CN
  - 50x EM4100 Chip-Karten [🛒 18€](https://amzn.to/2JDfh7g)
  - OnOff Shim [🛒 18€]

## Software
  - Raspbian Buster Lite
  - Docker und Docker-Compose
  
## Container / Services
  -  [`droppy`](https://github.com/silverwind/droppy) Web-Dateimanager, (Port 8989, http)
  - [`mopidy`](https://mopidy.com/) Music Player:
    - Spielt Medien ab
    - [Iris-Extension](https://mopidy.com/ext/iris/) (WebUI) zur manuellen Steuerung (Port 80, http)
    - [MPD-Extension](https://mopidy.com/ext/mpd/) zur Steuerung per MPD-Protokoll  (Port 6600)
  - [`rfidac`](https://git.in-ulm.de/verchow/jambox/src/master/setup/roles/rfidac/templates)
    - Liest den USB RDIF Reader
    - Führt Bash-Skipts mit der Tag ID als Namen aus
    - Steuert mopidy per mpd (mpc)
    - Aktualisiert Tag-Ordner im Medien-Verzeichnis

## Volumes
  - `/home/pi/dudelbox/mopidy/data/mopidy/music/`
    - `rfidac` legt Ordner mit der Tag-ID hier an
    - `mopidy` spielt die Medien hier ab
    - `droppy` lässt Dateien hier hochladen, verschieben, löschen und editieren
    
## Tag Actions
  - Die eingesetzten 125 kHz Tags kodieren eine 10-stellige Nummer (ID)
  - Für jeden erkannten Tag wird ein Bash-Skript mit dem Namen der ID ausgeführt, z.B. 0012482569
  - Sollte das Script noch nicht vorhanden sein, wird es als Kopie von `_action_tempalte` erstellt
  - Das Skript kann jetzt angepasst werden um z.B.:
    - Die Lautstäre zu regeln oder die Wiedergabe zu stoppen
    - Den Pi herunterzufahren
    - Eine bestimmte URI oder Playlist abzuspielen
  - Ohne Anpassung werden vom Skript folgende Aktionen ausgeführt:
    - Im Medien-Ordner wird eine Ordner mit Tag-ID als Namen angelegt (falls noch nicht vorhanden)
    - Der Zeitstempel des Ordners wird aktuallisiert indem eine Datei `.lastplay` gelöscht und dann angelegt wird
    - Etwaig bestehende Titel in der Playlist werden gelöscht und etwaig gespielte Titel gestoppt
    - Alle Medien im Ordner werden der Playliste hinzugefügt und abgespielt

## Konfiguration
Siehe `.env` für die jeweilgen Container

## Glosar 
  - Tag: RFID Transponder, z.B. Karte oder Fob