#!/bin/bash

# Used documentation
# https://www.sigmdel.ca/michel/ha/rpi/bluetooth_n_buster_01_en.html
# https://www.sigmdel.ca/michel/ha/rpi/bluetooth_01_en.html

# Update system and install software
sudo apt update && sudo apt --yes upgrade
sudo apt install --yes bluealsa

# Allow user pi to use bluetooth
sudo adduser pi bluetooth

# Enable as a2dp source and sink
sudo sed -e '/^ExecStart.*/s/$/ -p a2dp-source -p a2dp-sink/' \
         -i /lib/systemd/system/bluealsa.service

# Disable SIM access profile
sudo sed -e '/^ExecStart.*/s/$/ --noplugin=sap/' \
         -i /lib/systemd/system/bluetooth.service

# Load new kernel/bt modules, apply group access and restart services
sudo reboot

## Interactive 
# sudo bluetoothctl
# scan on
# scan off
# pair 50:18:06:52:53:C5
# connect 50:18:06:52:53:C5
# trust 50:18:06:52:53:C5
# quit
# btid="50:18:06:52:53:C5"


# Test Playback
aplay -D bluealsa:DEV=$btid /usr/share/sounds/alsa/Noise.wav

# Set system-wide default device and test again
# Alternative same file works in the in user context: ~/.asoundrc
cat <<EOF | sudo tee /etc/asound.conf
pcm.!default {
        type plug
        slave.pcm {
                type bluealsa
                device "$btid"
                profile "a2dp"
        }
}
EOF

aplay /usr/share/sounds/alsa/Noise.wav
