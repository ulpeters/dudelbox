docker run -it --rm --privileged --device=/dev/snd:/dev/snd --name mpd -p 6600:6600 -p 8000:8000 arm32v6/alpine
apk --no-cache add mpd mpc ncmpc ffmpeg alsa-utils


mkdir -p /var/lib/mpd/music
mkdir -p /var/lib/mpd/.mpd/playlists
touch /var/lib/mpd/.mpd/database
touch /var/lib/mpd/.mpd/state
chown -R mpd:audio /var/lib/mpd/


cat <<EOF | tee /etc/asound.conf
defaults.pcm.card 1
defaults.ctl.card 1
}
EOF

cat <<EOF | tee /etc/asound.conf
pcm.!default {
        type plug
        slave {
                pcm "hw:1,0"
        }
}

ctl.!default {
        type hw
        card 1
}
EOF

cat <<EOF | tee /etc/mpd.conf
music_directory         "~/music"
playlist_directory      "~/.mpd/playlists"
db_file                 "~/.mpd/database"
log_file                "/dev/stdout"
pid_file                "~/.mpd/pid"
state_file              "~/.mpd/state"
sticker_file            "~/.mpd/sticker.sql"

user                    "mpd"
group                   "audio"

bind_to_address         "any"
port                    "6600"

log_level               "default"

auto_update             "yes"
follow_outside_symlinks "yes"
follow_inside_symlinks  "yes"

zeroconf_enabled        "no"

audio_output {                                                                            
        type            "alsa"                                                            
        name            "Device"                                                     
}
EOF

su mpd -s /bin/sh -c \
"wget -O /var/lib/mpd/music/toto.wav https://www.ee.columbia.edu/~dpwe/sounds/music/africa-toto.wav"

cp /usr/share/sounds/alsa/Noise.wav /var/lib/mpd/music/

mpc update && mpc ls | mpc add && mpc play

/usr/bin/mpd --no-daemon --stdout &
fg

