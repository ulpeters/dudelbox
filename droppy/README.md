# Droppy
### Introduction
[Droppy](https://github.com/silverwind/droppy) is a web-based filemanager to upload, delete, move and edit files and view media directly in the web browser.
  
### Get started
1. Update your `data` dir, `uid` and `gid` to run the service and used as permissions for new files in [`.env`](.env)
2. Turn on authentication in [data/config/config.json](data/config/config.json) (optional)
2. `docker-compose up -d`
3. Open `http://<hostname>:8989` in your preferred webbrowser

### Alternatives
Droppy is no longer supported, potential alternatives are:
- [FileGator](https://github.com/filegator/filegator)
- [File Browser](https://hub.docker.com/r/filebrowser/filebrowser)
