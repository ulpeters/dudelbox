#!/bin/bash

# https://dev.to/rohansawant/installing-docker-and-docker-compose-on-the-raspberry-pi-in-5-simple-steps-3mgl
curl -sSL https://get.docker.com | sh
sudo usermod -aG docker pi
sudo apt-get install -y libffi-dev libssl-dev python3 python3-pip git
sudo pip3 install docker-compose

# https://github.com/docker-library/official-images#architectures-other-than-amd64
# https://hub.docker.com/u/arm32v7/

# Tests / Examples
docker run --rm arm32v6/bash date
docker run -d --rm arm32v6/bash sleep 100
docker ps
docker run -it --rm arm32v6/alpine date
