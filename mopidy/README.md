# Mopidy Media Server

Mopidy runs under uid 100 with the gid defined in the build ARG `gid_audio`.

## Devices/Volumes
  - Sound card: `/dev/snd:/dev/snd`
  - Mopidy home: `./data/mopidy/:/var/lib/mopidy/`

## Ports
  - 6600 MPD
  - 6680 Mopidy Web Interface, set to 80 in docker-compose.yml

## Configuration
  - The configuration assumes that a USB audio device exists as card 1 in alsa, a different configuration can be set in:
    - `/data/mopidy/.config/mopidy/mopidy.conf`
    - ``data/etc/asound.conf`
  - Set build ARG `gid_audio` to the gid of group `audio` on the docker host (default 29)