#!/bin/bash


PS3="Select disk to install Raspbian: "
select disk in `lsblk --include 8 --nodeps --output NAME --noheadings`
do
  echo "________________________________________________________________________"
  lsblk --nodeps /dev/$disk --output TRAN,VENDOR,MODEL,HOTPLUG
  echo "________________________________________________________________________"
  lsblk /dev/$disk --output NAME,TYPE,FSTYPE,SIZE,LABEL,MOUNTPOINT
  echo "________________________________________________________________________"
  read -p "ARE YOU SURE TO OVERRIDE THIS DISK? (y/n) " -n 1 -r
  echo #newline
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "Proceeding with $sdcard"
    sdcard=/dev/$disk
    break
  fi
done

for part in \
  `findmnt  --output SOURCE,TARGET --raw | grep $sdcard | cut -d" " -f2`
  do sudo umount --verbose $part
done


