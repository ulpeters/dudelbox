#!/bin/bash

# Get latest Raspbian image and checksum file 
img_url_regex='http:\/\/downloads\.raspberrypi\.org\/raspbian_lite\/images\/raspbian_lite-[0-9-]{10}\/[0-9-]{10}-raspbian-buster-lite\.zip'
img_url=`curl --silent https://downloads.raspberrypi.org/raspbian_lite_latest | \
grep --extended-regexp --ignore-case --only-matching $img_url_regex`
sha256_url=$img_url.sha256
image=`basename $img_url`
sha256=`basename $sha256_url`

# Download latest image and checksum file if not present yet
mkdir -p ~/images && cd ~/images
if [[ -f "$image" && -f "$sha256" ]]; then
  echo "Latest Raspbian image found: $image"
else
  echo "Downloading latest Raspbian..."
  curl --continue-at - --remote-name-all $img_url $sha256_url
fi

# Test checksum and exit if fails
echo -n "Testing checksum... "
set -e && sha256sum --check  $sha256 || exit 1
set +e


# Guess SDcard Reader
sdcard=`find /dev/disk/by-id/ -iregex '.*Card.*0$'`
sudo fdisk -l $sdcard

read -p "Continue with this disk? " -n 1 -r
echo #newline
if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "Proceeding with $sdcard"
else
  set -e && exit 1
fi


for part in `find /dev/disk/by-id/ -iregex '.*Card.*-part.*'`;
  do sudo umount --verbose $part && /bin/true;
done

unzip -p $image *.img \
| sudo dd of="$sdcard" bs=4K status=progress


# https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

# https://www.raspberrypi.org/documentation/remote-access/ssh/


chown -R 1000:1000 /media/$USER/rootfs/home/pi/.ssh

sync
for part in `find /dev/disk/by-id/ -iregex '.*Card.*-part.*'`;
  do umount --verbose $part && /bin/true;
done
