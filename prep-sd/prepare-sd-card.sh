#!/bin/bash

# this script is not intended to be used without modifications!

# ----------------------------------------------------------------------
# Get latest Raspbian image and checksum file 
img_url_regex='http:\/\/downloads\.raspberrypi\.org\/raspbian_lite\/images\/raspbian_lite-[0-9-]{10}\/[0-9-]{10}-raspbian-buster-lite\.zip'
img_url=`curl --silent https://downloads.raspberrypi.org/raspbian_lite_latest | \
grep --extended-regexp --ignore-case --only-matching $img_url_regex`
sha256_url=$img_url.sha256
image=`basename $img_url`
sha256=`basename $sha256_url`

# Download latest image and checksum file if not present yet
mkdir -p ~/images && cd ~/images
if [[ -f "$image" && -f "$sha256" ]]; then
  echo "Latest Raspbian image found: $image"
else
  echo "Downloading latest Raspbian..."
  curl --continue-at - --remote-name-all $img_url $sha256_url
fi

# Test checksum and exit if fails
echo -n "Testing checksum... "
set -e && sha256sum --check  $sha256 || exit 1
set +e

# ----------------------------------------------------------------------
# Select the disk
PS3="Select disk to install Raspbian: "
select disk in `lsblk --include 8 --nodeps --output NAME --noheadings`
do
  echo "________________________________________________________________________"
  lsblk --nodeps /dev/$disk --output TRAN,VENDOR,MODEL,HOTPLUG
  echo "________________________________________________________________________"
  lsblk /dev/$disk --output NAME,TYPE,FSTYPE,SIZE,LABEL,MOUNTPOINT
  echo "________________________________________________________________________"
  read -p "ARE YOU SURE THAT YOU WANT TO OVERWRITE THIS DISK? (y/n) " -n 1 -r
  echo #newline
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "Proceeding with $sdcard"
    sdcard=/dev/$disk
    break
  fi
done


# ----------------------------------------------------------------------
# Testing parameters / hostname
name=$1

if [ "X$name" == "X" ]; then
  echo "Kein Hostname für die Box angegeben!"
  echo "USAGE: $0 name"
  read -p "Hostname jetzt eingeben?: " name
fi

echo "Hostname: $name"
echo "Image: $image"


# ----------------------------------------------------------------------
# Preparing the disk

# Unmounting partitions
for part in \
  `findmnt  --output SOURCE,TARGET --raw | grep $sdcard | cut -d" " -f2`
  do sudo umount --verbose $part
done

# Write image to sdcard
unzip -p $image *.img \
| sudo dd of="$sdcard" bs=4K status=progress
sync

echo 
echo Rausnehmen, kurz warten, reinstecken und mount abwarten, dann Enter
read -n 1 -p "Geht's weiter?" unused

# Configure WiFi
# https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md
cp ~/wpa_supplicant.conf /media/$USER/boot/

# Enable / Setup SSH
# https://www.raspberrypi.org/documentation/remote-access/ssh/
touch /media/$USER/boot/ssh

mkdir -p /media/$USER/rootfs/home/pi/.ssh
chmod 0700 /media/$USER/rootfs/home/pi/.ssh

cp ~/.ssh/id_rsa.pub /media/$USER/rootfs/home/pi/.ssh/authorized_keys
chmod 0600 /media/$USER/rootfs/home/pi/.ssh/authorized_keys

chown -R 1000:1000 /media/$USER/rootfs/home/pi/.ssh

# Setting hostname
echo $name | sudo tee /media/$USER/rootfs/etc/hostname
echo 127.0.1.2 $name | sudo tee -a /media/$USER/rootfs/etc/hosts

# Set timezone
echo "Europe/Berlin" \
  | sudo tee  /media/$USER/rootfs/etc/timezone
sudo ln -sf /usr/share/zoneinfo/Europe/Berlin \
  /media/dominik/rootfs/etc/localtime
   

# Set locales
sudo sed -i -e '/^# en_US\.UTF-8 UTF-8/s/^# //' \
            -e '/^# de_DE\.UTF-8 UTF-8/s/^# //' \
            /media/$USER/rootfs/etc/locale.gen
#sudo locale-gen # need to run at first boot
#idea: https://github.com/nmcclain/raspberian-firstboot
#sudo update-locale LANG=de_DE.UTF-8 LC_MESSAGES=en_US.UTF-8 #equivalent
echo -e "LANG=de_DE.UTF-8\nLC_MESSAGES=en_US.UTF-8" \
  | sudo tee /media/$USER/rootfs/etc/default/locale 

# Unmounting partitions
sync
for part in \
  `findmnt  --output SOURCE,TARGET --raw | grep $sdcard | cut -d" " -f2`
  do sudo umount --verbose $part
done
