# RFID Acton Controller (rfidac)
## Credits
The idea and most code was borrowed from [here](https://git.in-ulm.de/verchow/jambox/src/master/setup/roles/rfidac/templates/), thanks Thomas!

## Introduction
The rdicac is the central piece of user interaction which reads the USB card reader and takes action. The full picture is explained in the [dudelbox readme](../README.md). This container runs under uid 100 with the gid set in the Dockerfile build ARG `gid_input` (default 105). The gid should represent the group `input` on the Docker host to make `/dev/input/event0` accessible. 

## Script Overview
- `rfid-test.py` simplified script that prints detected tag IDs to the docker log
- `rfidac.py` reads the rfid tags and runs respective bash scripts with threshold when the same tag was detected twice
- - - `_action_template` is the template copied when no corresponding bash script was found
- `_mpd_helper` is a collection of common functions sourced by the bash scripts

