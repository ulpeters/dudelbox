#! /usr/bin/python3

import os
import subprocess
import logging
from shutil import copyfile
from datetime import datetime
from evdev import InputDevice, ecodes

logging.basicConfig(level=logging.DEBUG)

def read_rfid():
    combined_string = ""
    for event in InputDevice("/dev/input/event0").read_loop():
        if event.type == ecodes.EV_KEY and event.value == 0:  # value 0 = release key
            if event.code == 28:  # code 28 = KEY_ENTER
                return combined_string
            # [4:5]? .. KEY[] starts with 'KEY_' and we expect one char
            combined_string += ecodes.KEY[event.code][4:5] 

last_rfid = None
last_ts = datetime.now()
while True:
    rfid = read_rfid()
    seconds_gone = (datetime.now() - last_ts).total_seconds()
    print(rfid)
    last_ts = datetime.now()
    last_rfid = rfid
