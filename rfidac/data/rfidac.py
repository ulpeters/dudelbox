#! /usr/bin/python3

"""
rfid action commander deamon
"""

import os
import subprocess
import logging
from shutil import copyfile
from datetime import datetime
from evdev import InputDevice, ecodes

logging.basicConfig(level=logging.DEBUG)


def read_rfid():
    combined_string = ""
    for event in InputDevice("/dev/input/event0").read_loop():
        if event.type == ecodes.EV_KEY and event.value == 0:  # value 0 = release key
            if event.code == 28:  # code 28 = KEY_ENTER
                return combined_string
            # [4:5]? .. KEY[] starts with 'KEY_' and we expect one char
            combined_string += ecodes.KEY[event.code][4:5] 


def run_action(script: str):
    logging.info(f"RFID action: run {script}")
    try:
        r = subprocess.call(["/bin/bash", script])
    except Exception as err:
        logging.info(err)
        last_rfid = None


last_rfid = None
last_ts = datetime.now()
while True:
    rfid = read_rfid()
    seconds_gone = (datetime.now() - last_ts).total_seconds()

    script = f"/opt/{rfid}"

    if os.path.islink(script):  # special RFID - alsways run!
        logging.info(f"RFID {rfid} is special. {script} is a link")
        run_action(script)
        continue

    if not os.path.exists(script):
        copyfile("/opt/_action_template", script)
        logging.info(f"RFID action: empty {script} created from template")
        continue

    if rfid == last_rfid and seconds_gone <= 5:
        logging.info(f"Same RFID after {seconds_gone:.1f} seconds. Do nothing.")
        continue

    last_ts = datetime.now()
    last_rfid = rfid
    run_action(script)
